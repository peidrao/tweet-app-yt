# Twitter - Django YT

# 1 -  Configurações do ambiente

Criar ambiente virtual

> pip install virtualenv

>python3 -m virtualenv [nome]

Instalar django

> pip install django

Criar super usuário

> python manage.py createsuperuser

Criar core da aplicação 
> django-admin startproject core .```

## 1.1 - Configuração de mídias

Importar os

Criar duas pastas "projeto/static-server" e "projeto/src/static-storage"

**Static-serve**: Mídias do Django

**Static-storage**: Mídias do projeto

configurações no settings

```
STATICFILES_DIRS = [ 
    os.path.join(BASE_DIR, 'static-storage')
]

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static-serve')

```

# 2 - Criando aplicação -> tweets

1. Adicionando nos apps em settings
2. criando primeiro model
3. Associando ao usuário

# 3 - Criando CRUD  

Usar class based views


