from django.utils.timesince import timesince
from rest_framework import serializers

from tweets.models import Tweet
from accounts.api.serializers import UserDisplaySerializer

class TweetModelSerializer(serializers.ModelSerializer):
    user  = UserDisplaySerializer(read_only=True)
    date_display = serializers.SerializerMethodField()
    timesince = serializers.SerializerMethodField()
    class Meta:
        model = Tweet
        fields = [
            'user',
            'content',
            'created_at',
            'date_display',
            'timesince'
        ]
    
    def get_date_display(self, obj):
        return obj.created_at.strftime('%d %b %I:%M %p')

    def get_timesince(self, obj):
        return timesince(obj.created_at) + " ago"

