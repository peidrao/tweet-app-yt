from django.urls import path
from django.views.generic.base import RedirectView
from .views import TweetDetailView, TweetListView, TweetCreateView, TweetUpdateView, TweetDeleteView

app_name = 'tweet'

urlpatterns = [
    path('', RedirectView.as_view(url='/')),
    path('search/', TweetListView.as_view(), name='list'),
    path('create/', TweetCreateView.as_view(), name='create'),
    path('<int:pk>/', TweetDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', TweetUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', TweetDeleteView.as_view(), name='delete'),
]

