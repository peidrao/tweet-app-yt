from django import forms

from .models import Tweet

class TweetModelForm(forms.ModelForm):
    content = forms.CharField(label='', widget=forms.Textarea(
        attrs={ 
        'placeholder': 'your tweet',
        'class': 'form-control'}
        ))
    class Meta:
        model = Tweet
        fields = [ 'content']
    
    def clean_content(self, *args, **kwargs):
        content = self.cleaned_data.get('content')
        return content