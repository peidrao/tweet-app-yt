from django.db import models
from django.conf import settings
from django.urls import reverse
from django.core.exceptions import ValidationError

# Create your models here.

def validate_content(value):
    content = value
    if content == "":
        raise ValidationError('Content não pode conter espaço em branco!')
    return value

class Tweet(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)
    content = models.CharField(max_length=140, validators=[validate_content])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return str(self.content)

    def get_absolute_url(self):
        return reverse("tweet:detail", kwargs={"pk": self.pk})
    