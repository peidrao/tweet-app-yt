from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.db.models import Q
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from .models import Tweet
from .forms import TweetModelForm
from .mixins import FormUserNeededMixin, UserOwnerMixin

class TweetCreateView(FormUserNeededMixin, CreateView):
    form_class = TweetModelForm
    template_name = 'tweets/create_tweet.html'
    success_url = reverse_lazy('home')
    login_url = '/admin/'


class TweetUpdateView(LoginRequiredMixin, UserOwnerMixin, UpdateView):
    model = Tweet
    form_class = TweetModelForm
    template_name = 'tweets/update_view.html'
    success_url = '/'


class TweetDeleteView(LoginRequiredMixin, DeleteView):
    model = Tweet
    template_name = 'tweets/delete_tweet.html'
    success_url = reverse_lazy('home')

class TweetDetailView(DetailView):
    template_name = 'tweets/detail_view.html'
    model = Tweet
    

    def get_object(self):
        id = self.kwargs.get('pk')
        return Tweet.objects.get(id=id)


class TweetListView(ListView):
    template_name = "index.html"
    model = Tweet
    ordering = ['-created_at']
    

    def get_queryset(self, *args, **kwargs):
        qs = Tweet.objects.all()
        print(self.request.GET)
        query = self.request.GET.get('q', None)
        if query is not None:
            qs = qs.filter(Q(content__icontains=query) | Q(user__username__icontains=query))
        return qs
    

    def get_context_data(self, *args, **kwargs):
        context = super(TweetListView, self).get_context_data(*args, **kwargs)
        context['create_form'] = TweetModelForm()
        context['create_url'] = reverse_lazy('tweet:create')
        return context